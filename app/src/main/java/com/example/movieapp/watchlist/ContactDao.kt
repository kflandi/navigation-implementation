package com.example.movieapp.watchlist

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {

    @Upsert
    suspend fun upsertTitle(name: Name)

    @Delete
    suspend fun deleteTitle(name: Name)

    @Query("SELECT * FROM name ORDER BY title ASC")
    fun getTitlesOrderedByName(): Flow<List<Name>>



   // @Query("SELECT * FROM contact ORDER BY phoneNumber ASC")
    //fun getContactsOrderedByPhoneNumber(): Flow<List<Contact>>
}