package com.example.movieapp.watchlist

sealed interface ContactEvent {
    object SaveTitle: ContactEvent
    data class SetTitle(val title: String): ContactEvent
    //data class SetPhoneNumber(val phoneNumber: String): ContactEvent
    object ShowDialog: ContactEvent
    object HideDialog: ContactEvent
    data class SortTitles(val sortType: SortType): ContactEvent
    data class DeleteTitle(val name: Name): ContactEvent
}