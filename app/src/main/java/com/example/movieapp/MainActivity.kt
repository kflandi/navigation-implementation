package com.example.movieapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.currentRecomposeScope
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.room.Room
import com.example.movieapp.ui.UI.HomeContent
//import com.example.movieapp.ui.UI.App
import com.example.movieapp.ui.UI.Theme.MovieAppTheme
import com.example.movieapp.ui.UI.LogInScreen
//import com.example.movieapp.ui.UI.LoginApp
import com.example.movieapp.ui.UI.Screens
import com.example.movieapp.watchlist.ContactDatabase
import com.example.movieapp.watchlist.ContactScreen
import com.example.movieapp.watchlist.ContactViewModel


class MainActivity : ComponentActivity() {


    // Creates db
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            ContactDatabase::class.java,
            "titles.db"
        ).build()
    }
    //sets up viewModel
    private val viewModel by viewModels<ContactViewModel>(
        factoryProducer = {
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return ContactViewModel(db.dao) as T
                }
            }
        }
    )



//creates the screen for ContactScreen

    // A surface container using the 'background' color from the theme
    // val state by viewModel.state.collectAsState()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MovieAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val state by viewModel.state.collectAsState()
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = "screen_login") {

                        composable("screen_login") {
                            LogInScreen(navController)
                        }
                        composable("screen_home") {
                            HomeContent(navController)
                        }
                        composable("screen_add") {
                            ContactScreen(state = state, onEvent = viewModel::onEvent, navController)
                        }
                    }
                   // val state by viewModel.state.collectAsState()
                    //ContactScreenCall(state = state, onEvent = viewModel::onEvent,  onAddClicked = )
                    //LoginApp()

                }
            }
        }
    }
}



/*

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MovieAppTheme {
        //ShuffleScreen()
    }
}
@Preview(showBackground = true)
@Composable
fun LogInPreview() {
    MovieAppTheme (darkTheme = false){

        LogInScreen(onLoginSuccess = {})
    }
}
@Preview(showBackground = true)
@Composable
fun HomePreview() {
    MovieAppTheme (darkTheme = false){
        App()
    }
}
@Preview(showBackground = true)
@Composable
fun DarkLogInPreview() {
    MovieAppTheme (darkTheme = true){

        LogInScreen(onLoginSuccess = {})
    }
}
@Preview(showBackground = true)
@Composable
fun DarkHomePreview() {
    MovieAppTheme (darkTheme = true){
        App()
    }
}
*/
