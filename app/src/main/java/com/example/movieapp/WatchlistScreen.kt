package com.example.movieapp.ui.UI

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.movieapp.R
import com.example.movieapp.ui.UI.Theme.MovieAppTheme
import com.example.movieapp.watchlist.WatchlistContent

/*
@Composable
fun Watchlist() {
    var currentScreen by remember { mutableStateOf(Screens.Watchlist) }
    MovieAppTheme(darkTheme = true) {
        WatchlistScreen(currentScreens = currentScreen, onNavigate = { currentScreen = it })
    }
}
//SampleStart( onAddClicked = {onNavigate(Screens.Home)} )
@Composable
fun WatchlistScreen(
    currentScreens: Screens,
    onNavigate: (Screens) -> Unit
) {
    when (currentScreens) {
        Screens.Home -> HomeContent(onAddClicked = { onNavigate(Screens.Watchlist) })
        Screens.Watchlist -> WatchlistContent( onAddClicked = { onNavigate(Screens.Home)})
        Screens.Login -> null
    }
}

@Composable
fun WatchlistContent(onAddClicked: () -> Unit){
    val mediumPadding = dimensionResource(R.dimen.padding_medium)
    Scaffold (
        topBar = {
            WatchTopAppBar(modifier = Modifier.padding(bottom = 200.dp), onAddClicked = onAddClicked)
        }
    ){innerPadding ->
        Column (
            modifier = Modifier
                .padding(innerPadding)
        ){

        }

    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WatchTopAppBar(modifier: Modifier = Modifier, onAddClicked: () -> Unit){
    CenterAlignedTopAppBar(
        title = {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "Watchlist",
                    style = MaterialTheme.typography.displayMedium
                )
            }
        },
        navigationIcon = {
            IconButton(onClick = onAddClicked) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Back Button",
                    modifier = Modifier
                        .size(24.dp)
                )
            }
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.background,
            titleContentColor = MaterialTheme.colorScheme.primary,
            navigationIconContentColor = MaterialTheme.colorScheme.primary
        ),
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun WatchlistAppPreview() {
    MovieAppTheme (darkTheme = true){
        Watchlist()
    }
}

*/