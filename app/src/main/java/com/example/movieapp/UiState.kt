package com.example.movieapp.ui.UI

data class UiState(
    val isShowingHomepage: Boolean = true
)
