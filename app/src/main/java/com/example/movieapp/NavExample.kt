package com.example.movieapp

import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController


// example for button/page manuvering
@Composable
fun Log(navController: NavHostController) {

    Button(onClick = { navController.navigate("screen_home") }) {
        Text("Go to Screen Home")
    }

}



