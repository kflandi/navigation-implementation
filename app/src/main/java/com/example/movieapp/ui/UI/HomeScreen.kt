package com.example.movieapp.ui.UI



import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column

import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size

import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.movieapp.R
import com.example.movieapp.ui.UI.Theme.MovieAppTheme


/*
@Composable
fun App() {
    var currentScreen by remember { mutableStateOf(Screens.Home) }
    MovieAppTheme(darkTheme = true) {
        HomeScreen(currentScreens = currentScreen, onNavigate = { currentScreen = it })
    }
}
@Composable
fun HomeScreen(
    currentScreens: Screens,
    onNavigate: (Screens) -> Unit
) {
    when (currentScreens) {
        Screens.Home -> HomeContent(onAddClicked = { onNavigate(Screens.Watchlist) })
        Screens.Watchlist -> WatchlistContent( onAddClicked = { onNavigate(Screens.Home)})// Placeholder for your Add Screen content
        Screens.Login -> null
    }
}
*/
@Composable
fun HomeContent(navController: NavHostController){
    val mediumPadding = dimensionResource(R.dimen.padding_medium)
    Scaffold (
        topBar = {
            MovieTopAppBar(modifier = Modifier.padding(bottom = 200.dp), navController)
        }
    ){innerPadding ->
        Column(
            modifier = Modifier
                //.statusBarsPadding()
                .verticalScroll(rememberScrollState())
                .padding(innerPadding),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                text = "What To Watch Next?",
                style = typography.titleLarge,
                modifier = Modifier.padding(bottom = 5.dp),
                color = MaterialTheme.colorScheme.primary
            )
            HomeLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(mediumPadding)
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(mediumPadding),
                verticalArrangement = Arrangement.spacedBy(mediumPadding),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Button(
                    modifier = Modifier.fillMaxWidth(),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary
                    ),
                    onClick = { }
                ) {
                    Text(
                        text = "???",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontSize = 16.sp
                    )
                }

            }
        }
    }
}
@Composable
fun HomeLayout(modifier: Modifier) {
    val mediumPadding = dimensionResource(R.dimen.padding_medium)

    Card(
        modifier = Modifier
            .size(width = 350.dp, height = 200.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer
        )
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(mediumPadding),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(mediumPadding)
                .wrapContentSize(Alignment.Center)
        ) {
            Text(
                text = "???",
                textAlign = TextAlign.Center,
                style = typography.displayMedium,
                modifier = Modifier
                    .padding(top = 40.dp)
                    .fillMaxWidth()

            )
            Text(
                text = "Let Me Help With That",
                textAlign = TextAlign.Center,
                style = typography.titleMedium,
                modifier = Modifier
                    .padding(top = 30.dp)

            )

        }
    }

}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieTopAppBar(modifier: Modifier = Modifier, navController: NavHostController){
    CenterAlignedTopAppBar(
        title = {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = stringResource(R.string.app_name),
                    style = MaterialTheme.typography.displayMedium
                )
            }
        },

        actions = {
            IconButton(onClick = { navController.navigate("screen_add") }) {
                Icon(
                    imageVector = Icons.Filled.Add,
                    contentDescription = "add",
                    modifier = Modifier
                        .size(24.dp)
                )
            }
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.background,
            titleContentColor = MaterialTheme.colorScheme.primary,
            actionIconContentColor = MaterialTheme.colorScheme.primary
        ),
        modifier = modifier
    )
}
/*
@Preview(showBackground = true)
@Composable
fun MovieAppPreview() {
    MovieAppTheme (darkTheme = true){
        App()
    }
}

 */