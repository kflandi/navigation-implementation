Caleb Elkevizth:

What have you contributed to the project between the last check in and this check in?
    So far we have been largely focused on conceptual work, coming up with what exactly we want the app to do and how we want the app to look.  Considering 
this there was not a great deal of work done on code for the program, although Isaiah did create a Composable function for the main screen.  Most recently
we all worked on creating mock-ups of the app's 3 screens and I put together the Login Screen layout.
What have you learned between the last check in and this check in?
    So far we have learned how to create all of the components necessary for a basic Android app UI.  We have gone over a variety of containers from rows
to boxes.  Within these containers we've added static text and images as well as buttons and text boxes to take in user input.  All of these things are 
the foundation of any app and will certainly be a core part of our project.
What do you need help with?
    Everything so far is going quite smoothly but I will likely need some instruction when it comes to the login process.  I am not sure how we are
going to record and check user's logins and right now it is what I am most concerned about coding.
What do you expect to complete between the next check in and this check in?
    I believe that we will have all of the UI figured out and implemented by the next check in.  We may not have the final versions of the UI finished
but I think we will definitely have created a decent UI for the 3 different screens our app will have.


Isaiah Ramirez:  
**What have you contributed to the project between the last check in and this check in?**  
	- I worked on the layout and design for the main screen for our app. I also helped plan out and design the other aspects and features that we want to implement into our app. 

**What have you learned between the last check in and this check in?**  
	- We figured out a good system that works for all of us to make sure everyone is on the same page about the end goal of our app and we have a clear idea of what we want.

**What do you need help with?**
	- I think as of right now there's not much that we really need help with, but I'm sure as we work on it more and try to implement more stuff down the road we will need some help.

**What do you expect to complete between the next check in and this check in?**
	- I'm hoping that by our next check we can have a lot of the UI figured out and hopefully have some actual functionality in our app even if not extremely complex.

Kayden Landis:
*What have you contributed to the project between the last check in and this check in?*
From our project proposal, I have helped design the layout of our list screen that users can access, created and set up a general template for our project to follow on Google Docs, and researched tutorials on things that we can add to our project. 

*What have you learned between the last check in and this check in?*
I have learned a few new design concepts within Android Studio Jetpack Compose. This includes simple animations for UI elements, useful list implementations, and how to properly use Gitlab.

*What do you need help with?*
Mainly I need help looking for useful sources to better learn a wider range of Jetpack Compose functions and features to use in our code. I’ve found a lot of helpful Youtube tutorials, but most of them focus on similar things we have already learned in class.

*What do you expect to complete between the next check in and this check in?*
I want to try and help set up our main file to match our template doc. Essentially this would be adding in each page of our app (Login, Main, List) and then adding a general idea of what we would like each UI element to look like before we begin adding in what they do. 
