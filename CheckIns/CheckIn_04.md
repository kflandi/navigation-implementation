Caleb Elkevizth:
**What have you contributed to the project between the last check in and this check in?**
I have continued work on the login screen and have added a default username and password
which are checked when the login button is pressed.  I improved the UI of the login screen
in a few ways and we should be able to easily implement navigation into the login screen
now.

**What have you learned between the last check in and this check in?**
I have learned how to pull information for our apps from the internet, and that figuring
out theming and colors is a bit more of a pain than I realized.

**What do you need help with?**
Right now I am feeling pretty good about our progress with the app however I currently 
could use help with getting the username/password error to function properly and update the
UI how I want.

**What do you expect to complete between the next check in and this check in?**
I expect that between this check in and the next we will all have added our screens to the
project and will be able to link them all through navigation.

Isaiah Ramirez:
**What have you contributed to the project between the last check in and this check in?**  
- I completed navigation for our app so now when the appropiate buttons are clicked the user can correctly move between the difference screens within our app.  

**What have you learned between the last check in and this check in?**  
- At first I had a lot of issues trying to get the navigation to work between the homescreen and the watchlist screen within our app but after I did get it to finally work I could tell that I definitely felt more confident in my understanding of navigation. When it came time to implement the navigation for the login screen I did still run into a few issues but I got them fixed relatively quick and because I already had a better understanding of navigation it didn't take me nearly as long to implement as it did the first time.  

**What do you need help with?**  
- I think our app is in a really good place right now, I think we are pretty close to having everything completed for our app. As of right now I don't think there is really anything that we need help with.  

**What do you expect to complete between the next check in and this check in?**  
- All the navigation is done so I'm hoping by the next check in we can have everything for the watchlist screen completed so the user can add and remove items to their watchlist.  


Kayden Landis: 

__What have you contributed to the project between the last check in and this check in?__

Between this check in and the last, I have almost finished changing the structure of the displayed scrollable list that contains all of the users input. All I have left to finish is implementing a less buggy add button and to change the color scheme to fit the rest of the project. 

__What have you learned between the last check in and this check in?__

I have learned how to correctly implement a scrollable list of user input that has a delete function added to it. Other than this, I got a better understanding of how to store user entered data and how to effectively swap and display different screens within an app.

__What do you need help with?__

I think I finally have found enough resources and tutorials for Android Studio to have a pretty good idea of how to finish and add my list implementation to our app's third page. I will let you know if I stumble into anything odd or confusing.

__What do you expect to complete between the next check in and this check in?__

I expect to complete the 3rd page of our app that can record user input and display it to a scrollable list. If I have enough time, I’ll start to work on the button we will need for the main page that selects a random element from the list and displays the results above it.
