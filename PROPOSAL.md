# P1: Project Proposal

**Group Members:**
	Alexander Elkevizth, Isaiah Ramirez, Kayden Landis

**Communication:**
	Our group will primarily use discord as our mode of communication

**Meeting time:**
	We will meet on Tuesdays at 11:30 in the library weekly to work on the project throughout the semester. 

**App Idea:**
	Our idea for an app is one that will help people choose what show/movie to watch next. The user can log into the app and then can click a shuffle button of some sort that will randomly show them a show or movie, the user can either add the movie or show to a watchlist or they can click the shuffle button again and get a new recommendation. The user will also be able to edit the watchlist and remove shows and movies from it as they finish them.